# API NODEJS

API NodeJS, Express, Sequelize and MySQL.

## Getting Started

1. Install dependencies:

   ```bash
   npm install

2. Run API

    ```bash
    npm run start:dev
    
    this command run API with nodemon for dev