'use strict';
const express = require('express');
const cors = require('cors');
const path = require('path');
require('dotenv').config();
require('./src/models/relations');
const { users_route, post_route } = require('./src/routes');
const swagger = require('./swagger');

const PORT =process.env.PORT || 3006;
const app = express();

//Cors config
app.use(cors());

app.use(express.json({limit:'50mb'}));
app.use(express.urlencoded({extended: true, limit: '50mb'}));

const baseUrl = '/api';
app.use(`${baseUrl}/users`, users_route);
app.use(`${baseUrl}/posts`, post_route);
swagger(app);
app.listen(PORT, (err) => {
    if (err) {
        throw new Error(err);
    }
    console.log(`API run on port ${PORT}`);
});
