const postService = require('../services/posts.service');

class UserController {
    async createPost(req, res){
        try {
            const userId = req.body.userId;
            const data = {
                idea_sumary: req.body.idea_sumary,
            }

            const posts = await postService.create(userId, data);
            res.status(200).json({
                isValid: true,
                message: 'Posts create sucess',
                data: posts
            });
        } catch (error) {
            res.status(400).json({
                isValid: false,
                message: error,
            });
        }
    }

    async updatePost(req, res){
        try {
            const id = req.params.id;
            const data = await postService.update(id, req.body);
            res.status(200).json({
                isValid: true,
                message: 'Update success posts',
                data: data
            });
        } catch (error) {
            res.status(400).json({
                isValid: false,
                message: `Update post fail ${error}`,
            });
        }
    }

    async deletePost(req, res){
        try {
            const id = req.params.id;
            await postService.delete(id);
            res.status(200).json({
                isValid: true,
                message: 'Post deleted success'
            });
        } catch (error) {
            res.status(400).json({
                isValid: false,
                message: `Delete posts fail ${error}`,
            });
        }
    }

    async getById(req, res) {
        try {
            const id = req.params.id;
            const posts = await postService.findById(id);
            res.status(200).json({
                isValid: true,
                message: 'Posts  success',
                data: posts
            });
        } catch (error) {
            res.status(400).json({
                isValid: false,
                message: `No se encontro posts ${error}`,
            });
        }
    }

    async getAllPost(req, res){
        try {
            const posts = await postService.findAllPost();
            res.status(200).json({
                isValid: true,
                message: 'Posts list success',
                data: posts
            });
        } catch (error) {
            res.status(400).json({
                isValid: false,
                message: error,
            });
        }
    }
}

module.exports = new UserController();