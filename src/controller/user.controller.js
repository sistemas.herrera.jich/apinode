const userService = require('../services/user.service');
const { v4:uuidv4 } = require('uuid');

class UserController {
    async createUser(req, res){
        try {
            let uuidD = uuidv4();
            const data = {
                user_first_name: req.body.user_first_name, 
                user_last_name: req.body.user_last_name,
                uuid: uuidD
            }

            const user = await userService.create(data);
            res.status(200).json({
                isValid: true,
                message: 'Users create sucess',
                data: user
            });
        } catch (error) {
            res.status(400).json({
                isValid: true,
                message: 'Users create fail',
            });
        }
    }

    async updateUser(req, res){
        try {
            const id = req.body.userId;
            const data = {
                user_first_name: req.body.user_first_name, 
                user_last_name: req.body.user_last_name,
            };

            const user = await userService.update(id, data);

            res.status(200).json({
                isValid: true,
                message: 'Users update sucess',
                data: user
            });
        } catch (error) {
            res.status(400).json({
                isValid: true,
                message: 'Users update fail',
            });
        }
    }

    async deleteUser(req, res){
        try {
            const id = req.params.id;
            const user = await userService.delete(id);

             res.status(200).json({
                isValid: true,
                message: 'Users and posts delete sucess',
                data: user
            });
        } catch (error) {
            res.status(400).json({
                isValid: false,
                message: 'Users delete fail',
            });
        }
    }

    async findById(req, res){
        try {
            const id = req.params.id;
            const user = await userService.findById(id);
            res.status(200).json({
                isValid: true,
                message: 'User success found',
                data: user
            });
        } catch (error) {
            res.status(400).json({
                isValid: false,
                message: 'Users not found',
            });
        }
    }

    async findAll(req, res){
        try {
            const users = await userService.findAll();
            res.status(200).json({
                isValid: true,
                message: 'Users success found',
                data: users
            });
        } catch (error) {
            res.status(400).json({
                isValid: false,
                message: 'Users not found',
            });
        }
    }
}

module.exports = new UserController();