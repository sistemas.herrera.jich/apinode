const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(
    process.env.BD_NAME,
    process.env.BD_USER,
    '',
    {
        host: process.env.BD_HOST,
        dialect: 'mysql',

        pool: {
            max: 5,
            min: 0,
            acquire: 3000,
            idle: 10000
        }
    }
);

module.exports = sequelize;