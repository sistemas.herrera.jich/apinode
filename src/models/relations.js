const sequelize = require("../db/db");
const Posts = require("./table1");
const Users = require("./table2");

Users.hasMany(Posts, { foreignKey: 'posted_by', onDelete: 'CASCADE'});
Posts.belongsTo(Users, { foreignKey: 'posted_by'});

sequelize.sync().then(()=>{
    console.log('Tables creates sucess');
});