const Sequelize = require('sequelize');
const sequelize = require('../db/db');
const Users = require('./table2');
const Posts = sequelize.define(
    "posts",
    {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        idea_sumary: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        posted_by: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model:Users,
                key: 'id',
            }
        }

    },
    {
        timestamps: true,
    }
);

module.exports = Posts;