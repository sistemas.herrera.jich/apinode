const Sequelize = require('sequelize');
const sequelize = require('../db/db');

const Users = sequelize.define(
    'users',
    {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        user_first_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        user_last_name: {
            type: Sequelize.STRING,
            allowNull:false
        },
        uuid: {
            type: Sequelize.UUID,
            allowNull: false
        }
    }
);

module.exports = Users;