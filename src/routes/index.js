const users_route = require('./user.route');
const post_route = require('./posts.route');
module.exports = {
    users_route,
    post_route
};