'use strict'
const express = require("express");
const postController = require("../controller/post.controller");
const {
    postSchema,
    postSchemaUpdate,
    postSchemaUpdateParams,
    validate,
    validateParams
} = require("../validation/schemas.validation");
const router = express.Router();

/**
 * @swagger
 * tags:
 *      name: Post
 *      description: post controller crud
 */
router.post('/create', validate(postSchema),postController.createPost);
router.put('/update/:id', validateParams(postSchemaUpdateParams), validate(postSchemaUpdate), postController.updatePost);
router.delete('/delete/:id', validateParams(postSchemaUpdateParams), postController.deletePost);
router.get('/getById/:id', postController.getById);
router.get('/getAll', postController.getAllPost);

module.exports = router;