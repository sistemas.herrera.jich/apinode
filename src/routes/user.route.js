'use strict'
const express = require("express");
const userController = require("../controller/user.controller");
const { userSchema, userSchemaUpdate, validate } = require("../validation/schemas.validation");
const router = express.Router();

/**
 * @swagger
 *  /api/users/create:
 *  post:
 *    tags: 
 *      - User create
 *    description: Create new user
 *    parameters:
 *    - name: user_first_name
 *      description: User First name
 *    - name: user_last_name
 *      description: User Last name
 *    responses:
 *      200:
 *       description: Instance of user created
 */
router.post('/create', validate(userSchema),userController.createUser);
router.put('/update', validate(userSchemaUpdate), userController.updateUser);
router.delete('/delete/:id', userController.deleteUser);
router.get('/getById/:id', userController.findById);
router.get('/getAll', userController.findAll);

module.exports = router;