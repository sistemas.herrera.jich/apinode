const { Sequelize } = require("sequelize");
const Posts = require("../models/table1");
const Users = require("../models/table2");

class PostService{
    async create(userId, data){
        try {
            const user = await Users.findByPk(userId);
            if (!user) {
                throw new Error('User not found');
            }
            return await Posts.create({...data, posted_by:user.id});
        } catch (error) {
            throw new Error(error);
        }
    }

    async update(id, data){
        try {
            const isExist = await Posts.findByPk(id);
            
            if(!isExist) throw new Error('User not found');

            await Posts.update(data, { where: {id: id }});
            const postUpdate = await Posts.findByPk(id);
            return postUpdate;
        } catch (error) {
            throw new Error(error);
        }
    }

    async delete(id){
        try {
            const isExist = await Posts.findByPk(id);
            
            if (!isExist) throw new Error('User not found');

            return await Posts.destroy({ where: {id: id}});
        } catch (error) {
            throw new Error(error);
        }
    }

    async findAllPost(){
        try {
            const posts = await Posts.findAll(
                { include:[{
                    model: Users,
                    as: 'user',
                    where: { id: Sequelize.col('posts.posted_by') },
                    attributes: ['user_first_name', 'user_last_name']
                }]}
            );

            return posts;
        } catch (error) {
            throw new Error(error);
        }
    }

    async findById(id){
        try {
            const posts = await Posts.findByPk(id,
                { include:[{
                    model: Users,
                    as: 'user',
                    where: { id: Sequelize.col('posts.posted_by') },
                    attributes: ['user_first_name', 'user_last_name']
                }]}
            );

            return posts;
        } catch (error) {
            throw new Error(error);
        }
    }
}

module.exports = new PostService();