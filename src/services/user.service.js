const Users = require("../models/table2");

class UserService {
    async create(data){
        try {
            return await Users.create(data);
        } catch (error) {
            throw new Error(error)
        }
    }

    async update(id, data){
        try {
            const user = await Users.findByPk(id);
            if (!user) {
                throw new Error('User not found');
            }

            await Users.update(data, { where: {id: id }});
            const userUpdate = await Users.findByPk(id);
            return userUpdate;
        } catch (error) {
            console.log(error);
            throw new Error(error)
        }
    }

    async delete(id){
        try {
            const user = await Users.findByPk(id);
            if (!user) {
                throw new Error('User not found');
            }

            return await Users.destroy({ where: {id: id}});
        } catch (error) {
            throw new Error(error)
        }
    }

    async findById(id){
        try {
            return await Users.findByPk(id);
        } catch (error) {
            throw new Error(error)
        }
    }
    
    async findAll(){
        try {
            const users = await Users.findAll();
            return users;
        } catch (error) {
            throw new Error(error)
        }
    }


}

module.exports = new UserService();