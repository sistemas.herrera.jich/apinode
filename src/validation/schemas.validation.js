const Joi = require("joi");

const userSchema = Joi.object({
    user_first_name: Joi.string().required(),
    user_last_name: Joi.string().required(),
});

const userSchemaUpdate = Joi.object({
    user_first_name: Joi.string().required(),
    user_last_name: Joi.string().required(),
    userId: Joi.number().required(),
});

const postSchema = Joi.object({
    idea_sumary: Joi.string().required(),
    userId: Joi.number().required(),
});

const postSchemaUpdate = Joi.object({
    idea_sumary: Joi.string().required(),
});

const postSchemaUpdateParams = Joi.object({
    id: Joi.number().required(),
});

function validate(schema) {
    return (req, res, next) => {
    const { error } = schema.validate(req.body);
        if (error) {
            res.status(400).json({ error: error.details[0].message });
        } else {
            next();
        }
    };
}
function validateParams(schema) {
    return (req, res, next) => {
    const { error } = schema.validate(req.params);
        if (error) {
            res.status(400).json({ error: error.details[0].message });
        } else {
            next();
        }
    };
}

module.exports = {
    userSchema,
    userSchemaUpdate,
    postSchema,
    postSchemaUpdate,
    postSchemaUpdateParams,
    validate,
    validateParams
}

