const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const PORT =process.env.PORT || 3006;
const swaggerOptions = {
    swaggerDefinition: {
        
      info: {
        version: '1.0.0',
        title: 'Neon API Crud',
        description: 'API CRUD Test Neon Domain',
        contact: {
          name: '',
        },
        servers: [`http://localhost:${PORT}`],
      },
    },
    apis: ['./src/**/*.js'],
  };

  const swaggerDocs = swaggerJsDoc(swaggerOptions);

  module.exports = (app) => {
    app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
  }